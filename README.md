#using Python 2.7.12

# My project's README
#initial setup

#navigate to folder where code is cloned

cd findprivy_project/

python manage.py makemigrations

python manage.py migrate

./manage.py loaddata initial_data.json

#to runserver
python manage.py runserver


#URL to call API:

localhost:portnumber/sms/

#default:

localhost:8000/sms/
