from django.contrib import admin

# Register your models here.
from .models import Bank,Sender,Logic

@admin.register(Bank)
class BankAdmin(admin.ModelAdmin):
    list_display = (
        u'id',
        u'name'
                    )

@admin.register(Sender)
class SenderAdmin(admin.ModelAdmin):
    list_display = (
        u'id',
        u'name'
    )

@admin.register(Logic)
class LogicAdmin(admin.ModelAdmin):
    list_display = (
        u'id',
        u'title',
        u'regex'
    )