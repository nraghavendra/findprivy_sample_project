from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Bank(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return "Bank:%s" %(self.name)

class Logic(models.Model):
    title = models.CharField(max_length=50)
    regex = models.CharField(max_length = 512)

    def __unicode__(self):
        return "Title:%s" %(self.title)

class Sender(models.Model):
    name = models.CharField(max_length=50)
    bank = models.ForeignKey(Bank)
    logic = models.ForeignKey(Logic)

    def __unicode__(self):
        return "Sender:%s" %(self.name)