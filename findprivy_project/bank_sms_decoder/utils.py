import json
class HttpUtils():
    @staticmethod
    def parse_request_body(request):
        #import pdb;pdb.set_trace()
        parsed_data = {}
        try:
            if request.body:
                parsed_data = json.loads(request.body)

        except:

            parsed_data = {}


        return parsed_data