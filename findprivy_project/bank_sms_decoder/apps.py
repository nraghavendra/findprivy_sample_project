from __future__ import unicode_literals

from django.apps import AppConfig


class BankSmsDecoderConfig(AppConfig):
    name = 'bank_sms_decoder'
