from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.sms_decoder, name='sms_decoder'),
]