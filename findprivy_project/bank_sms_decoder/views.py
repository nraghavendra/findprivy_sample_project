from django.shortcuts import render
import re
from .models import Sender
import json
from django.http.response import HttpResponse
from .utils import HttpUtils
from datetime import datetime
import string

# Create your views here.
def sms_decoder(request):
    request_body = HttpUtils.parse_request_body(request)
    sender_name = request_body.get('sender')
    message = request_body.get('body')
    sender = Sender.objects.get(name = sender_name)
    regex = sender.logic.regex
    reg_ex = re.compile(regex)
    decoded_info = reg_ex.findall(message)
    output = {}
    output.update({'success':'false',
                   'amount':None,
                   'bank':sender.bank.name,
                   'datetime':None,
                   'merchant':None
                   })
    if len(decoded_info) == 3:
        output['success'] = 'true'
    for each in decoded_info:
        for index in range(0,3):
            if each[index]:
                value = check_value(index)
                output[value] = each[index]

    if output['amount']:
        amount_str = output['amount']
        amount_str = string.replace(amount_str,',','')
        amount_flt = float(amount_str)
        if amount_flt.is_integer():
            amount_int = int(float(amount_str))
            output['amount'] = amount_int
        else:
            output['amount'] = amount_flt

    if output['datetime']:
        datetime_str = output['datetime']
        if len(datetime_str)==9:
            datetime_obj = datetime.strptime(datetime_str, '%d-%b-%y')
        else:
            datetime_obj = datetime.strptime(datetime_str,'%Y-%m-%d:%H:%M:%S')
        datetime_str = datetime_obj.strftime('%d-%b-%y %H:%M:%S')
        output['datetime'] = datetime_str
    return HttpResponse(json.dumps(output), content_type='application/json')

def check_value(index):  #using generealized regular expressions /
                        # i.e extracting in order- amount,datetime & merchant
    value = ''
    if index==0:
        value = 'amount'
    elif index == 1:
        value = 'datetime'
    elif index == 2:
        value = 'merchant'
    return value